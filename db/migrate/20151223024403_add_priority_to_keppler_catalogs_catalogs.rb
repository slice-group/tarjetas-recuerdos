class AddPriorityToKepplerCatalogsCatalogs < ActiveRecord::Migration
  def change
    add_column :keppler_catalogs_catalogs, :priority, :integer
  end
end
