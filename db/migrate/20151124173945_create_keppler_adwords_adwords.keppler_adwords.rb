# This migration comes from keppler_adwords (originally 20151103152402)
class CreateKepplerAdwordsAdwords < ActiveRecord::Migration
  def change
    create_table :keppler_adwords_adwords do |t|
      t.string :name
      t.text :description
      t.string :url
      t.text :script

      t.timestamps null: false
    end
  end
end
