# Agregar datos de configuración
KepplerContactUs.setup do |config|
	config.mailer_to = "info@tarjetasyrecuerdos.com"
	config.mailer_from = "info@tarjetasyrecuerdos.com"
	config.name_web = "Targetas y Recuerdos"
	#Route redirection after send
	config.redirection = "/#footer"


	# Agregar keys de google recaptcha
	Recaptcha.configure do |config|
	  config.public_key  = "6LeZxhETAAAAAKQQjMzjC17ObmSwBEsNBdVdmZfc"
	  config.private_key = "6LeZxhETAAAAAHEegXXco_tsgAA2xpGx5wqwYnLj"
	end
end