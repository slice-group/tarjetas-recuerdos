class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
	
  def index
  	@sections = KepplerCatalogs.sections
  	@message = KepplerContactUs::Message.new
  end

  def show_product
  	@sections = KepplerCatalogs.sections
  	@catalogs = KepplerCatalogs::Catalog.all.where(section: params[:product]).order(priority: :asc)
    @message = KepplerContactUs::Message.new
    @subcategories = []
    @catalogs.each do |category|
      unless category.subcategory.nil? or category.subcategory.blank?
        @subcategories << category.subcategory
      end
    end
    @subcategories = @subcategories.uniq
  end
  def show_product_by_subcategory
    @sections = KepplerCatalogs.sections
    @catalogs = KepplerCatalogs::Catalog.all.where(section: params[:product], subcategory: params[:subcategory].gsub("+", " ")).order(priority: :asc)
    @message = KepplerContactUs::Message.new
    @subcategories =[]
    KepplerCatalogs::Catalog.all.where(section: params[:product]).order(priority: :asc).each do |category|
      unless category.subcategory.nil? or category.subcategory.blank?
        @subcategories << category.subcategory
      end
    end    
    @subcategories = @subcategories.uniq
  end
end
