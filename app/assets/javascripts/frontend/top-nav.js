$(document).ready(function(){
	function removeHash () { 
	  history.pushState("", document.title, window.location.pathname + window.location.search);
	}

	$('.top-nav').onePageNav({
	    currentClass: 'current',
	    changeHash: false,
	    scrollSpeed: 500,
	    scrollThreshold: 0.5,
	    filter: ':not(.external)',
	    easing: 'swing',
	    begin: function() {

	    },
	    end: function() {   

	    },
	    scrollChange: function($currentListItem) {      
	      		  
	    }
	});	

	var num = parseInt($('#home').css('height').split('px')[0]) - 10;

	$(window).bind('scroll', function() {
	  if ($(window).scrollTop() > num) {
	    $('#home-navbar').addClass('navbar-fixed-top');
	    $('#home-navbar').css({'height':'54px'});
	    $('#front-navbar').addClass('navbar-fixed-top');
	    $('#show-logo').css({'display': 'block'});
	  } else {
	    $('#home-navbar').removeClass('navbar-fixed-top');
	    $('#home-navbar').css({'height':'12%'});
	    $('#front-navbar').removeClass('navbar-fixed-top');
	    $('#show-logo').css({'display': 'none'});
	  }
	});
});